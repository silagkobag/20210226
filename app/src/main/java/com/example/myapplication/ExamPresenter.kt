package com.example.myapplication

class ExamPresenter (private val callback: Callback) {

    interface Callback {
        fun onNextQuestion(question: Question?)
        fun onScoreUpdate(score: Int)
    }

    private var score = 0
    private val questionBank = QuestionBank()
    private var question: Question? = null

    fun nextQuestion() {
        question = questionBank.next()
        callback.onNextQuestion(question)
    }

    fun judgeAnswer(answer:Int): Boolean {
        if (question == null) return false

        if (answer == question!!.correctChoice) {
            score += 1
            callback.onScoreUpdate(score)
            return true
        } else {
            return false
        }
    }
}