package com.example.myapplication

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity(), ExamPresenter.Callback {

    private lateinit var tvQuestion: TextView
    private lateinit var rgItems: RadioGroup
    private lateinit var rbItem1: RadioButton
    private lateinit var rbItem2: RadioButton
    private lateinit var rbItem3: RadioButton
    private lateinit var rbItem4: RadioButton
    private lateinit var btOk: Button
    private lateinit var tvScore: TextView

    private val examPresenter = ExamPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvQuestion = findViewById(R.id.tvQuestion)
        rgItems = findViewById(R.id.rgItems)
        rbItem1 = findViewById(R.id.rbItem1)
        rbItem2 = findViewById(R.id.rbItem2)
        rbItem3 = findViewById(R.id.rbItem3)
        rbItem4 = findViewById(R.id.rbItem4)
        btOk = findViewById(R.id.btOk)
        tvScore = findViewById(R.id.tvScore)

        examPresenter.nextQuestion()
        tvScore.text = "答對：0"

        btOk.setOnClickListener { v ->
            when (rgItems.checkedRadioButtonId) {
                R.id.rbItem1 -> judgeAnswer(0, v.context)
                R.id.rbItem2 -> judgeAnswer(1, v.context)
                R.id.rbItem3 -> judgeAnswer(2, v.context)
                R.id.rbItem4 -> judgeAnswer(3, v.context)
            }

            examPresenter.nextQuestion()
            rgItems.clearCheck()
        }
    }

    private fun judgeAnswer(a: Int, context: Context) {
        if (examPresenter.judgeAnswer(a)) {
            Toast.makeText(context, "答對", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, "答錯", Toast.LENGTH_LONG).show()
        }
    }

    override fun onNextQuestion(question: Question?) {
        if (question == null) {
            tvQuestion.text = ""
            rbItem1.text = ""
            rbItem2.text = ""
            rbItem3.text = ""
            rbItem4.text = ""
        } else {
            tvQuestion.text = question.question
            rbItem1.text = question.choices[0]
            rbItem2.text = question.choices[1]
            rbItem3.text = question.choices[2]
            rbItem4.text = question.choices[3]

        }
    }

    override fun onScoreUpdate(score: Int) {
        tvScore.text = "答對：${score}"
    }
}