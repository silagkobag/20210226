package com.example.myapplication

data class Question(
    val question: String,
    val choices: ArrayList<String>,
    val correctChoice: Int
)