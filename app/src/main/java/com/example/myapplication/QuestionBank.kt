package com.example.myapplication

class QuestionBank {
    var questions = arrayListOf<Question>()
    private var nextIndex = 0

    init {
        val choices1 = arrayListOf("after", "by", "before", "on")
        val q1 = Question("Your book is _ the chair.", choices1, 3)

        val choices2 = arrayListOf("王維", "李白", "孟浩然", "杜甫")
        val q2 = Question("有詩仙稱號的是", choices2, 1)

        val choices3 = arrayListOf("Python", "C++", "C", "Java")
        val q3 = Question("請選出最早出現的程式語言", choices3, 2)

        questions.addAll(listOf(q1, q2, q3))
    }

    fun next(): Question? {
        if (nextIndex < questions.size) return questions[nextIndex++]
        else return null
    }
}